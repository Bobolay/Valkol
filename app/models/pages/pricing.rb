class Pages::Pricing < Page

  has_html_block :intro
  has_html_block :table
  has_html_block :second_text
end